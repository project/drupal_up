#! /bin/bash

# Get the FQDN

fqdn=`hostname -f`
local_ip=`hostname -I | awk '{print $2}'`
host_ip=`echo $local_ip|awk -F. '{print $1 "." $2 "." $3 ".1"}'`

# Only run if Drupal hasn't already been installed
if ! [ -e /etc/apache2/sites-available/$fqdn ] ; then

  # Install Drush
  apt-get --purge remove drush
  cd /usr/share/
  git clone --recursive --branch 8.x-6.x http://git.drupal.org/project/drush.git
  ln -s /usr/share/drush/drush.php /usr/bin/drush

  # Install debugging and testing tools
  drush -y dl drush_iq
  apt-get install php5-xdebug php5-curl
  cat > /etc/php5/conf.d/xdebug_drupal-up.ini <<EOL
xdebug.default_enable=1
xdebug.remote_enable=1
xdebug.remote_handler=dbgp
xdebug.remote_host=${host_ip}
xdebug.remote_port=56456
xdebug.remote_autostart=0
EOL

  # Download Drupal
  cd /var/www
  cp /vagrant/drupal.git ./drupal -r
  #git clone --recursive --branch 8.x http://git.drupal.org/project/drupal.git
  chown www-data:www-data drupal -R
  cd drupal
  git checkout 8.x
  git pull

  # Install Drupal
  drush -y site-install standard --db-url=mysql://root:@localhost/drupal-up --site-name=Drupal-up --sites-subdir=$fqdn

  # Set a default password
  cd sites/$fqdn
  drush user-password admin --password="admin"

  # Fix permissions
  sudo /bin/bash /vagrant/files/fix-permissions.sh /var/www/drupal/ www-data

  # Configure Apache
  a2dissite default
  a2enmod rewrite
  cp /vagrant/files/drupal.conf /etc/apache2/conf.d/
  cp /vagrant/files/vhost /etc/apache2/sites-available/$fqdn
  sed -i "s/www.example.com/$fqdn/g" /etc/apache2/sites-available/$fqdn
  a2ensite $fqdn
  apache2ctl graceful

fi

echo '/////////////////////////////////////////////////'
echo '   You can visit your site at:'
echo "       http://$fqdn"
echo '       Username: admin'
echo '       Password: admin'
echo '/////////////////////////////////////////////////'
