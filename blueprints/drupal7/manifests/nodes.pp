node "drupal7" {
  include drush-vagrant::users

  exec {'run script':
    command   => '/bin/bash /vagrant/files/install.sh',
    logoutput => true,
  }


}
